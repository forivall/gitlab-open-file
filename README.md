# GitLab Open File

## Features
Open files from commit views, pull requests and more, quickly and easily, on your local machine.

![screenshot](https://gitlab.com/yumoose/gitlab-open-file/raw/master/screenshot.png)

- GitHub and GitLab repo support
- Line number jumping
- Multiple project support

## Supported Editors
Supports opening files in the following editors:
- Visual Studio Code
- Atom
- RubyMine

Favourite editor not listed? Make a pull request or open an issue with the label 'Editor Support'.

## Project Support
A project will need to be set up by configuring it's repository and local directory.

### Repository
The URL of the repository you want to be able to open files from (eg. https://gitlab.com/yumoose/gitlab-open-file).
This can be either a GitHub or GitLab repository.

### Local Directory
This is the local directory of your checked out project. Be sure to include the absolute path to your directory. (eg. /Users/yumoose/Projects/gitlab-open-file/).


