const webExtension = chrome || browser;

function findCurrentProject(projects) {
  return projects.find(function(project) {
    return window.location.href.match(project.repository);
  });
}

function handleWithGitLab(project) {
  // Doesn't actually check the project, checks the current document
  let headElements = Array.from(document.head.children);

  return headElements.some((elem) => { return elem.content === "GitLab"; });
}

function handleWithGitHub(project) {
  return window.location.href.match('https://github.com/');
}

function buttonHandleStrategy(project) {
  if (handleWithGitLab(project)) {
    return new GitLabButtonHandler;
  } else if (handleWithGitHub(project)) {
    return new GitHubButtonHandler;
  }
}

webExtension.storage.local.get(function (settings) {
  let editorProtocol = settings.protocol;
  let projects = settings.projects;
  let currentProject = findCurrentProject(projects);

  if (!editorProtocol || !currentProject) return;

  let projectDirectory = currentProject.directory || settings.directory;  // `settings.directory` is deprecated

  if (buttonHandler = buttonHandleStrategy(currentProject)) {
    buttonHandler.addButtons(document, editorProtocol, projectDirectory);
  }
});
