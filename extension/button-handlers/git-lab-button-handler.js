class GitLabButtonHandler extends ButtonHandler {
  addButtons(document, editorProtocol, projectDirectory) {
    this.addOpenFileButton(document.body, editorProtocol, projectDirectory);

    const observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        for (let i = 0; i < mutation.addedNodes.length; i++) {
          const newNode = mutation.addedNodes[i];

          if (!this.isFileOpenNode(newNode) && this.tagName == 'DIV')
            this.addOpenFileButton(newNode, editorProtocol, projectDirectory);
        }

        if (mutation.removedNodes.length > 0) {
          if (!Array.from(mutation.removedNodes).some((node) => {
            return this.isElementNode(node) && (node.classList.contains('open-file-button') || node.classList.contains('fa-file-code-o') || this.hasFileOpenButtonAlready(node))
          })) {
            this.updateOpenFileButtons(editorProtocol, projectDirectory);
          }
        }
      });
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  }

  findLineNumber(node) {
    let lineNumberAttribute = Array.from(node.attributes).find((attr) => attr.name === 'data-line-number' || attr.name === 'data-linenumber')

    if (lineNumberAttribute) {
      return lineNumberAttribute;
    } else {
      let childNodes = Array.from(node.children);

      for(let i = 0; i <= childNodes.length - 1; i++) {
        let childNode = childNodes[i];
        let childLineNumber = this.findLineNumber(childNode);

        if (childLineNumber) {
          return childLineNumber;
        }
      }
    }
  }

  updateOpenFileButtons(editorProtocol, projectDirectory) {
    let openFileButtons = document.getElementsByClassName('open-file-button');
    Array.from(openFileButtons).forEach((openFileButton) => {
      let parentNode = openFileButton.parentNode;

      parentNode.removeChild(openFileButton);
      this.addOpenFileButton(parentNode, editorProtocol, projectDirectory)
    });

    Array.from(document.getElementsByClassName('tooltip fade show')).forEach((tooltip) => {
      tooltip.parentNode.removeChild(tooltip);
    })
  }

  addOpenFileButton(node, editorProtocol, projectDirectory) {
    if (this.isElementNode(node) && this.isCopyToClipboardButton(node) && !this.hasFileOpenButtonAlready(node.parentNode)) {
      let openFileButton = document.createElement('button');
      openFileButton.classList.add('open-file-button', 'btn', 'btn-transparent', 'prepend-left-5', 'btn-clipboard');
      openFileButton.setAttribute('style', 'position: relative; bottom: 2px;');
      openFileButton.setAttribute('data-toggle', 'tooltip');
      openFileButton.setAttribute('data-placement', 'bottom');
      openFileButton.setAttribute('data-container', 'body');
      openFileButton.setAttribute('data-title', 'Open file in editor');
      openFileButton.setAttribute('data-original-title', 'Open file in editor');
      openFileButton.setAttribute('aria-label', 'Open file in editor');

      let filename = JSON.parse(node.getAttribute('data-clipboard-text'))['text'];
      let absoluteFilename = projectDirectory + filename;
      let lineNumber = 0

      let fileHolder = node.parentNode;
      while (!fileHolder.classList.contains('file-holder') || fileHolder.tagName === 'BODY') {
        fileHolder = fileHolder.parentNode;
      }

      if (fileHolder.tagName !== 'BODY') {
        let foundlineNumber = this.findLineNumber(fileHolder)

        if (foundlineNumber) {
          lineNumber = foundlineNumber.value;
        }
      }

      let href = protocolFor(editorProtocol).buildUrl(absoluteFilename, lineNumber);

      openFileButton.setAttribute('onclick', `location.href='${href}'`);

      let codeIcon = document.createElement('i');
      codeIcon.classList.add('fa', 'fa-file-code-o');
      codeIcon.setAttribute('aria-hidden', 'true');
      openFileButton.appendChild(codeIcon);

      node.parentNode.appendChild(openFileButton);
    } else {
      for (let i = 0; i < node.childNodes.length; i++) {
        this.addOpenFileButton(node.childNodes[i], editorProtocol, projectDirectory);
      }
    }
  }

  isElementNode(node) {
    return node.nodeType === node.ELEMENT_NODE;
  }

  isCopyToClipboardButton(node) {
    return node.matches('button.btn.btn-clipboard') && node.getAttribute('data-title') === 'Copy file path to clipboard';
  }

  hasFileOpenButtonAlready(node) {
    return Array.from(node.childNodes).some((child) => this.isFileOpenNode(child));
  }

  isFileOpenNode(node) {
    node.getAttribute && node.getAttribute('data-title') === 'Open file in editor'
  }
}
